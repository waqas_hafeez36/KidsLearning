package edu.bzu.www.myapplication;

import android.media.AudioManager;
import android.media.MediaPlayer;

import android.media.SoundPool;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class AlphabetSound extends Activity implements View.OnClickListener {
	
	Button a,b,c,d,e,f,g,h;
	SoundPool sp;
	int explode=0;
	MediaPlayer mp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	
		setContentView(R.layout.activity_alphabet_sound);
		
		
		
		a=(Button) findViewById(R.id.aBtn);
		b=(Button) findViewById(R.id.bBtn);
		c=(Button) findViewById(R.id.cBtn);
		d=(Button) findViewById(R.id.dBtn);
		e=(Button) findViewById(R.id.eBtn);
		f=(Button) findViewById(R.id.fBtn);
		g=(Button) findViewById(R.id.gBtn);
		h=(Button) findViewById(R.id.hBtn);
		
		a.setOnClickListener(this);
		b.setOnClickListener(this);
		c.setOnClickListener(this);
		d.setOnClickListener(this);
		e.setOnClickListener(this);
		f.setOnClickListener(this);
		g.setOnClickListener(this);
		h.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		switch(arg0.getId()){
		case R.id.aBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.aa);
           // mp = MediaPlayer.create(getApplicationContext(), R.raw.a); 
            mp.start();
			break;
		case R.id.bBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.bb);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.b); 
            mp.start();
			break;
		case R.id.cBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.cc);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.c); 
            mp.start();
			break;
		case R.id.dBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.dd);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.d); 
            mp.start();
			break;
		case R.id.eBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.ee);
           //mp = MediaPlayer.create(getApplicationContext(), R.raw.e); 
            mp.start();
			break;
		case R.id.fBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.ff);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.f); 
            mp.start();
			break;
		case R.id.gBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.gg);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.g); 
            mp.start();
			break;
		case R.id.hBtn:
            mp = MediaPlayer.create(AlphabetSound.this, R.raw.hh);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.h); 
            mp.start();
			break;
		}
	}
	
}
