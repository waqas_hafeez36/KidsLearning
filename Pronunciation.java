package edu.bzu.www.myapplication;

import android.media.MediaPlayer;
import android.os.Bundle;

import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;


public class Pronunciation extends Activity implements View.OnClickListener {

	Button a,b,c,d,e,f,g,h;
	MediaPlayer mp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_pronunciation);
		
		a=(Button) findViewById(R.id.aBtn);
		b=(Button) findViewById(R.id.bBtn);
		c=(Button) findViewById(R.id.cBtn);
		d=(Button) findViewById(R.id.dBtn);
		e=(Button) findViewById(R.id.eBtn);
		f=(Button) findViewById(R.id.fBtn);
		g=(Button) findViewById(R.id.gBtn);
		h=(Button) findViewById(R.id.hBtn);
		
		a.setOnClickListener(this);
		b.setOnClickListener(this);
		c.setOnClickListener(this);
		d.setOnClickListener(this);
		e.setOnClickListener(this);
		f.setOnClickListener(this);
		g.setOnClickListener(this);
		h.setOnClickListener(this);
		
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		switch(arg0.getId()){
		case R.id.aBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.a);
           // mp = MediaPlayer.create(getApplicationContext(), R.raw.a); 
            mp.start();
			break;
		case R.id.bBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.b);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.b); 
            mp.start();
			break;
		case R.id.cBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.c);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.c); 
            mp.start();
			break;
		case R.id.dBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.d);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.d); 
            mp.start();
			break;
		case R.id.eBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.e);
           //mp = MediaPlayer.create(getApplicationContext(), R.raw.e); 
            mp.start();
			break;
		case R.id.fBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.f);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.f); 
            mp.start();
			break;
		case R.id.gBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.g);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.g); 
            mp.start();
			break;
		case R.id.hBtn:
            mp = MediaPlayer.create(Pronunciation.this, R.raw.h);
            //mp = MediaPlayer.create(getApplicationContext(), R.raw.h); 
            mp.start();
			break;
		}
	}
}