package edu.bzu.www.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button btn=(Button)findViewById(R.id.btnSup);

        btn.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(RegisterActivity.this, SignupActivity.class);
                RegisterActivity.this.startActivity(myIntent);
                Toast.makeText(RegisterActivity.this, "Opening", Toast.LENGTH_SHORT).show();

            }
        });


        Button btn2=(Button)findViewById(R.id.btnLgin);

        btn2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myiIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                RegisterActivity.this.startActivity(myiIntent);
                Toast.makeText(RegisterActivity.this, "Opening", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
