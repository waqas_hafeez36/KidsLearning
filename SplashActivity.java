package edu.bzu.www.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);




        Button btn=(Button)findViewById(R.id.btnLearn);

        btn.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent myIntent = new Intent(SplashActivity.this, RegisterActivity.class);
                SplashActivity.this.startActivity(myIntent);
                Toast.makeText(SplashActivity.this, "Opening", Toast.LENGTH_SHORT).show();
            }
        });








    }
}
