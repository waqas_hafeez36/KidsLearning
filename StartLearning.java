package edu.bzu.www.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartLearning extends Activity implements View.OnClickListener {

	Button alphabetSoundBtn,pronunciationBtn,testSessionBtn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_learning);
		
		alphabetSoundBtn=(Button) findViewById(R.id.alphaSoundBtn);
		pronunciationBtn=(Button) findViewById(R.id.pronunBtn);
		testSessionBtn=(Button) findViewById(R.id.testBtn);
		
		alphabetSoundBtn.setOnClickListener(this);
		pronunciationBtn.setOnClickListener(this);
		testSessionBtn.setOnClickListener(this);
		
		alphabetSoundBtn.setTextColor(Color.WHITE);
		alphabetSoundBtn.setBackgroundColor(Color.BLACK);
		pronunciationBtn.setTextColor(Color.WHITE);
		pronunciationBtn.setBackgroundColor(Color.BLACK);
		testSessionBtn.setTextColor(Color.WHITE);
		testSessionBtn.setBackgroundColor(Color.BLACK);
	}
	
	@Override
	public void onClick(View arg0) {
		
		switch(arg0.getId()){
		case R.id.alphaSoundBtn:
			Intent i=new Intent(getApplicationContext(),Pronunciation.class);
			startActivity(i);
			break;
		case R.id.pronunBtn:
			Intent i1=new Intent(getApplicationContext(),AlphabetSound.class);
			startActivity(i1);
			break;
		case R.id.testBtn:
			Intent i2=new Intent(getApplicationContext(),TestSession.class);
			startActivity(i2);
			break;
		}
		
	}
	}
